<?php

// connection
$db = new PDO('mysql:host=localhost;dbname=stu_mn;charset=utf8mb4', 'root', '');

//build query
$query = "SELECT * FROM `students` ORDER BY first_name ASC";

//execution
$stmt = $db->query($query);
$students = $stmt->fetchAll(PDO::FETCH_ASSOC);

//$query = "SELECT * FROM `students` ORDER BY id DESC LIMIT 0,5";
$query = "SELECT * FROM `courses` ORDER BY title DESC";
//execution
$stmt = $db->query($query);
$courses = $stmt->fetchAll(PDO::FETCH_ASSOC); //


//manage posted data
if(strtolower($_SERVER['REQUEST_METHOD']) == 'post') {
    $student_id = $_POST['student_id'];
    $course_ids = $_POST['course_ids'];
    foreach($course_ids as $course_id){
        $query = "INSERT INTO `map_students_courses` ( `student_id`, `course_id`) VALUES ( '".$_POST['student_id']."', '".$course_id."');";
        $db->exec($query);
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<nav class="navbar navbar-default">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Brand</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="../student/index.php">View all students</a></li>
                <li><a href="../course/index.php">View all Courses</a></li>
                <li><a href="../course/create.html">Insert Courses</a></li>
                <li><a href="../student/create.html">Insert Students</a></li>
                <li><a href="../assign/create.php">Assign Courses</a></li>

            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>

<div class="container">
    <div class="row">
        <div class=" col-md-offset-3 col-md-6">
            <form method="post">
                <div class="form-group">
                    <label for="course_code">Select Student :</label>
                    <select name="student_id" id="student_id" class="form-control">

                        <option value="">Choose a Student</option>
                        <?php
                        foreach($students as $student):

                        ?>
                        <option value="<?=$student['id']?>"><?=$student['first_name']." ".$student['last_name']?></option>
                        <?php
                        endforeach;
                        ?>

                    </select>
                </div>
                <div class="form-group">
                    <label for="course_title">Select Course</label>
                    <select name="course_ids[]" id="course_ids" class="form-control" multiple size="6">

                        <option value="">Choose a Course</option>
                        <?php
                        foreach($courses as $course):

                            ?>
                            <option value="<?=$course['id']?>"><?=$course['title']?></option>
                            <?php
                        endforeach;
                        ?>

                    </select>
                </div>

                <button type="submit" class="btn btn-success">Submit</button>
            </form>
        </div>
    </div>
</div>



<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="../assets/js/bootstrap.min.js"></script>
</body>
</html>



